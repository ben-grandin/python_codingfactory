import base64
from src import const


# encode
def convert_string_to_list(s):
    """ 1) Convert string to list
    Args:
        s: A string to convert to a list
    Return:
        A list of with all letters from the string
    ["A", "B", "C", "D"]
    """
    return list(s)


def convert_list_to_int_list(string_as_a_list):
    """ 2) Convert string list to int list
    Args:
        string_as_a_list: A list of with all letters from the string
    Return:
        A list of int
    [65, 66, 67, 66]
    """
    return [ord(element) for element in string_as_a_list]


def convert_int_list_to_8_bits_list(int_list):
    """3) Convert int list to a 8 bits list
    Args:
        int_list: A list of with all letters from the string
    Return:
        A list of 8 bits string
    ["01000001", "01000010", "01000011", "01000100"]
    """
    return [bin(element).replace("b", "") for element in int_list]


def convert_list_to_string(an_8_bits_list):
    """
    4 & 9) Convert list to string
    Args:
        an_8_bits_list: A list of 6 bits string
    Return:
        A string of binary
    "01000001010000100100001101000100"
    """

    return "".join(an_8_bits_list)


def convert_string_to_bit_list(bit_string, size_of_the_bits):
    """
    5) Convert string to list with bits
    Args:
        bit_string: A list of with all letters from the string
        size_of_the_bits: The size of bits desired
    Return:
        A list of bits
        ["010000","010100","001001","000011","010001","000000"]
    """
    bit_list = []

    for i in range(0, len(bit_string), size_of_the_bits):
        # Parse bit_string to a bit string
        a_bit = bit_string[i: i + size_of_the_bits]

        if size_of_the_bits == 6 & len(bit_list) < 6:
            # fill bit_list with 0 until it have a length of 6
            a_bit = a_bit.ljust(6, "0")

        bit_list.append(a_bit)

    return bit_list


def convert_bit_list_to_unicode_list(bit_list):
    """
    6) Convert bit list to unicode list
    Args:
        bit_list: A list of 6 bits list
    Return:
        A list of 6 unicode
    [16, 20, 9, 3, 17, 0]
    """

    return list(map(convert_bit_to_int, bit_list))
    # return [int(a_bit, 2) for a_bit in list_with_6_bits]


def convert_int_list_to_base64_list(list_with_6_int):
    """
    7) Convert int list to base64 list
    la base 64
    Args:
        list_with_6_int: A list of 6 int
    Return:
        A list with base 64
        ["Q", "U", "J", "D", "R", "A"]
    """

    return [const.array_base64[a_6_int] for a_6_int in
            list_with_6_int]


def convert_base64_list_to_list_with_length_is_mutliple_of_4(
        base64_list):
    """
    8) Add "=" to a list until it length % 4 == 0
    Args:
        base64_list: A list with base 64
    Return:
        A list with base 64 and a length which is a multiple of 4
        ["Q", "U", "J", "D", "R", "A", "=", "="]
    """
    length = len(base64_list)
    missing_length = 4 - length % 4
    max_length = length + missing_length

    for i in range(max_length):
        if i >= length:
            base64_list.append("=")

    return base64_list
    # Another try with ternary but doesn't work
    #   return [i if i in base64_list else "=" for i in
    #       range(0, max_length)]


# Encode
def replace_char_from_string(string, char, replacer=""):
    """ Replace all char from a string
    Args :
        string: A string with all characters to replace
        char: the character to replace
        replacer: the replacer

    Return:
        A new string with replacers instead of old characters
        "QUJDRA"
    """
    return string.replace(char, replacer, string.count(char))


def convert_string_base64_to_list_int(string_base64):
    """ Convert string base64 to string int
    Args:
        string_base64: a string base64
    Return:
        a string with int equal to the index of base64
    [16, 20, 9, 3, 17, 0]
    """

    return [const.array_base64.index(base64) for base64 in string_base64]


def convert_list_int_to_list_6_bits(list_int):
    """ Convert string base64 to string int
        Args:
            list_int: a string base64
        Return:
            a list with int equal to the index of base64
        ["010000","010100","001001","000011","010001","000000"]
        """
    return [bin(an_int).replace("b", "").rjust(6, "0") for an_int in list_int]


def convert_list_to_string_with_length_mod_number_is_0(list_6_bits, x):
    """ Convert a list of 6 bits to a string with length multiple of a number
    Args:
        list_6_bits: A list of 6 bits
        x: the multiple of the length
    Return:
        A string with length multiple of a number
    """

    string = "".join(list_6_bits)
    return string[:-(len(string) % x)]


def unicode_list_to_convert_list_with_char(unicode_list):
    """
    7) Convert list base64 to int
    Args:
        unicode_list: A list with base 64
    Return:
        A list of 6 int
        ["Q", "U", "J", "D", "R", "A"]
    """

    return [const.unicode[int(unicode)] for unicode in unicode_list]


# Helpers
def convert_bit_to_int(a_bit):
    """ Convert a bit to int
    Args:
        a_bit: a char of 6 bits
    Return:
        A int
    """
    return int(a_bit, 2)

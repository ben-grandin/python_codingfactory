"""
Application base 64
"""

from src import main


def init(args):
    res = None
    user_input = open(args.input, "r").read() if args.input else args.s
    print("Base 64 encoder")

    if user_input == None:
        user_input = input("Enter a string to encode or X to exit: ")

    if args.e:
        res = main.encode_uri(user_input)
        print(f"encoded_uri: {res}")

    if args.d:
        res = main.decode_uri(user_input)
        print(f"decoded_uri: {res}")

    if args.output:
        f = open("examples/" + args.output, "w+")
        f.write(res)

    if args.m:
        # Exiting with input 'X' or 'x'
        if user_input.upper() == "X":
            print("Exiting base 64 encoder")
        else:
            init(args)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group2 = parser.add_mutually_exclusive_group()
    group.add_argument("-e", action="store_true",
                       help=
                       "Add this argument if you want to encode the string")
    group.add_argument("-d", action="store_true",
                       help=
                       "Add this argument if you want to decode the string")

    group2.add_argument("-m", action="store_true",
                        help=
                        "Add this argument if you want to "
                        "encode or decode multiple strings"
                        )
    group2.add_argument("-s",
                        help="Take the string you want to modify in argument")
    group2.add_argument("-input",
                        help="Take a file you want to encode or decode")
    parser.add_argument("-output",
                        help="Take a fie to write the respond")

    args_from_cmd_line = parser.parse_args()
    init(args_from_cmd_line)

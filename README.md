# Python: Encoder / Decoder Base64 

Project made during class

**Examples**:
- python3 base64codec.py -e -s ABCD

            encoded_uri: QUJDRA==

- python3 base64codec.py d -s QUJDRA==

            decoded_uri: ABCD

## Details

 Syntaxe    | Description                       | Incompatible with
 -----------| -----------                       | -----------
 -e         | Encode mode                       | -d
 -d         | Decode mode                       | -e
 -s         | String to convert                 | -s -m -input 
 -m         | Convert multiple input from string| -s -input
 -input     | Input file                        | -s -m
 -output    | Output file                       |

